﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buildings
{
    class Program
    {

        static void Main(string[] args)
        {
            double sumOfAreas=0;

            Person someone = new Person("Pesho Kalibrata");

            List<Room> ListOfRooms = new List<Room>();
            List<Room> ListOfRooms2ndhouse = new List<Room>();

            Bathroom bathroom = new Bathroom(30, "blue");
            Bedroom bedroom = new Bedroom(60, "purple");
            Kitchen kitchen = new Kitchen(44, "red");

            ListOfRooms.Add(bathroom);
            ListOfRooms.Add(bedroom);

            ListOfRooms2ndhouse.Add(bedroom);
            ListOfRooms2ndhouse.Add(kitchen);

            House house = new House( 2,"purvata kushta e sinq" ,someone, ListOfRooms);
            House secondhouse = new House(4, "vtorata kushta e zelena", someone, ListOfRooms2ndhouse);
            
            List<Building> listOfBuldings = new List<Building>();

            listOfBuldings.Add(house);
            listOfBuldings.Add(secondhouse);
            listOfBuldings.Add(secondhouse);

            Console.WriteLine("Area of first house: " +house.GetArea());
            Console.WriteLine("Area of second house: " + secondhouse.GetArea());


            foreach (Building h in listOfBuldings)
            {
                sumOfAreas += h.GetArea();
            }
            Console.WriteLine("Sum of Area of all houses: " + sumOfAreas);
            
            Console.ReadLine();
        }
    }

    
}
