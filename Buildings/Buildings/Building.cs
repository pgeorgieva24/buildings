﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buildings
{
    class Building   
    {

        private int area;
        private int height;
        private string color;


        public int Area
        {
            get
            {
                return area;
            }
            set
            {
                if (value >= 0)
                {
                    area = value;
                }
                else throw new ArgumentOutOfRangeException("Value for Building Area must be greater than 0");
            }
        }

        public int Height
        {
            get => height;
            set
            {
                if (value >= 0)
                {
                    height = value;
                }
                else throw new ArgumentOutOfRangeException("Value for Building Height must be greater than 0");
            }
        }

        public string Color
        {
            get => color;
            set
            {
                if (value.Length > 0)
                {
                    color = value;
                }
                else throw new ArgumentNullException("You didn't enter room color.");
            }
        }

        public virtual int GetArea()
        {
            return Area;
        }

        public Building( int height, string color)
        {
            //Area = buildingList
            Height = height;
            Color = color;
        }

        
        
    }
    
}
