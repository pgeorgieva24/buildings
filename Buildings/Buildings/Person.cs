﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buildings
{
    class Person 
    {
        private string name;

        public string Name
        {
            get => name;
            set
            {
                if (value.Length > 0)
                {
                    name = value;
                }
                else throw new ArgumentNullException("You didn't enter a name");
            }
        }

        public Person(string name)
        {
            Name = name;
        }
    }
}
