﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buildings
{
    class House:Building
    {
        public List<Room> RoomsInTheHouse;
        public Person Owner;
              
        public House(int height, string color, Person person, List<Room> ListOfRoomsInTheHouse):base ( /*area,*/  height,  color)  
        {
            Owner = person;
            RoomsInTheHouse = ListOfRoomsInTheHouse;
            Area= GetArea();
        }

        public override int GetArea()
        {
            int areaofhouse = 0;

            foreach (Room room in RoomsInTheHouse)
            {
                areaofhouse += room.Area;
            }
            return areaofhouse;
        }
        
    }
}
