﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buildings
{

    class Room
    {
        protected int area;
        protected string color;

        public int Area
        {
            get
            {
                return area;
            }
            set
            {
                if (value >= 0)
                {
                    area = value;
                }
                else throw new ArgumentOutOfRangeException("Value for Area of Room must be greater than 0.");
            }
        }

        public string Color
        {
            get => color;
            set
            {
                if (value.Length > 0)
                {
                    color = value;
                }
                else throw new ArgumentNullException("You didn't enter room color.");
            }
        }
        public Room(int area, string color)
        {
            Area = area;
            Color = color;
        }
    }
    

}
